packages:
  - docker.io

groups:
  - docker

system_info:
  default_user:
    groups: [docker]

runcmd:
  - /usr/bin/sleep 10
  - /usr/bin/docker ${image}
  - /usr/bin/docker run -d --restart=always ${ports} ${envs} ${image}