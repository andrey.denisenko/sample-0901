data "vkcs_compute_flavor" "compute" {
  name = var.vm_flavor
}

data "vkcs_images_image" "compute" {
  name = "Ubuntu-18.04-Standard"
}

locals {
  env = merge(var.env, {
    SPRING_PROFILES_ACTIVE: "vk",
  })
}

data "template_file" "script" {
  template = file("${path.module}/cloud-config.yaml.tpl")
  vars     = {
    image = var.image_name
    ports = "-p 80:${var.server_port}"
    envs  = join(" ", [for k, v in local.env : "-e ${k}=\"${v}\""])
  }
}

data "template_cloudinit_config" "config" {
  part {
    filename     = "init.cfg"
    content_type = "text/cloud-config"
    content      = data.template_file.script.rendered
  }
}

resource "vkcs_compute_instance" "compute" {
  name              = "${var.name}-vm"
  flavor_id         = data.vkcs_compute_flavor.compute.id
  image_id          = data.vkcs_images_image.compute.id
  availability_zone = var.vm_az

  key_pair = vkcs_compute_keypair.key.id

  user_data = data.template_cloudinit_config.config.rendered

  block_device {
    uuid                  = data.vkcs_images_image.compute.id
    source_type           = "image"
    destination_type      = "volume"
    volume_type           = "ceph-ssd"
    volume_size           = 8
    boot_index            = 0
    delete_on_termination = true
  }

  block_device {
    source_type           = "blank"
    destination_type      = "volume"
    volume_type           = "ceph-ssd"
    volume_size           = 8
    boot_index            = 1
    delete_on_termination = true
  }

  network {
    port = vkcs_networking_port.compute.id
  }

  depends_on = [
    vkcs_networking_network.net,
    vkcs_networking_subnet.compute
  ]
}

resource "vkcs_networking_secgroup" "compute" {
  name = "${var.name}-vm-secgroup"
}

resource "vkcs_networking_secgroup_rule" "compute_http" {
  direction         = "ingress"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = vkcs_networking_secgroup.compute.id
}

resource "vkcs_networking_secgroup_rule" "compute_ssh" {
  direction         = "ingress"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = vkcs_networking_secgroup.compute.id
}

resource "vkcs_networking_port" "compute" {
  name           = "${var.name}-port-compute"
  network_id     = vkcs_networking_network.net.id
  admin_state_up = true
  security_group_ids = [ vkcs_networking_secgroup.compute.id ]
  fixed_ip {
    subnet_id = vkcs_networking_subnet.compute.id
  }
}

resource "vkcs_networking_floatingip" "compute" {
  pool = data.vkcs_networking_network.ext.name
}

resource "vkcs_compute_floatingip_associate" "compute" {
  floating_ip = vkcs_networking_floatingip.compute.address
  instance_id = vkcs_compute_instance.compute.id
}
